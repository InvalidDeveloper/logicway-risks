const reducer = (state, action) => {

    if (state === undefined) {
        return {}
    }

    switch (action.type) {
        default:
            return state
    }

}

export default reducer