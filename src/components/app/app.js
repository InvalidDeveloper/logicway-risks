import React from 'react'
import {Switch} from 'react-router-dom'

import TopMenu from '../top-menu'

const App = () => {
    return (
        <div>
            <TopMenu/>
            <Switch>

            </Switch>
        </div>
    )
}

export default App
