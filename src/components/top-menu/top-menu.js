import React from 'react'
import classNames from 'classnames'
import {Link} from 'react-router-dom'

import style from './top-menu.module.scss'

const TopMenu = () => {

    const headerClass = classNames(style.header, 'row')
    const logoClass = classNames(style.logo, 'text-dark')
    const itemsClass = classNames(style.items, 'text-dark')

    return (
        <header role='banner' className={headerClass}>
            <Link to='/'>
                <div className={logoClass}>Main</div>
            </Link>
            <Link to='/table'>
                <div className={itemsClass}>Table</div>
            </Link>
        </header>
    )
}

export default TopMenu